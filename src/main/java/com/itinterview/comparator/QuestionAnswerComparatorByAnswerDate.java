package com.itinterview.comparator;

import java.util.Comparator;
import java.util.Date;

import com.itinterview.entity.QuestionAnswer;

public class QuestionAnswerComparatorByAnswerDate implements Comparator<QuestionAnswer>{

	@Override
	public int compare(QuestionAnswer o1, QuestionAnswer o2) {
		
		Date answerDate1= o1.getAnswer().getCreateddate();
		Date answerDate2= o2.getAnswer().getCreateddate();
		
		if((answerDate1!=null) && (answerDate2!=null)){
			return answerDate1.compareTo(answerDate2);
		}
		if((answerDate1== null) && (answerDate2!=null)){
			return -1;
		}
		if((answerDate1!=null) &&(answerDate2==null)){
			return 1;
		}
		
		return 0;
	}

}
