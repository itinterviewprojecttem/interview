package com.itinterview.comparator;

import java.util.Comparator;
import java.util.Date;

import com.itinterview.entity.Company;

public class CompanyComparatorByDate implements Comparator<Company>{

	@Override
	public int compare(Company company0, Company company1) {
		
		Date company0Date= company0.getCreateddate();
		Date company1Date= company1.getCreateddate();
		
		if((company0Date!=null) && (company1Date!=null)){
			return company0Date.compareTo(company1Date);
		}
		if((company0Date==null) && (company1Date!=null)){
			return -1;
		}
		if((company0Date!=null) && (company1Date==null)){
			return 1;
		}
		
		return 0;
	}
	
}
