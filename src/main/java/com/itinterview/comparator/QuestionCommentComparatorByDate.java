package com.itinterview.comparator;

import java.util.Comparator;
import java.util.Date;

import com.itinterview.entity.QuestionComment;

public class QuestionCommentComparatorByDate implements Comparator<QuestionComment>{

	@Override
	public int compare(QuestionComment questionComm1, QuestionComment questionComm2) {
		
		Date date1= questionComm1.getComment().getCreateddate();
		Date date2= questionComm2.getComment().getCreateddate();
		
		if((date1!=null) && (date2!=null)){
			return date1.compareTo(date2);
		}
		if((date1==null) && (date2!=null)){
			return -1;
		}
		if((date1!=null) && (date2==null)){
			return 1;
		}
		
		return 0;
	}

}
