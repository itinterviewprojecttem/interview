/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 *
 * @author user
 */
@Configuration
@EnableWebMvc
public class Config extends WebMvcConfigurerAdapter{
    
	private static final String WEBPAGE_PREFIX = "/WEB-INF/jsp/";
	private static final String WEBPAGE_SUFFIX = ".jsp";
	
	private static final String RESOURCE_HANDLER = "/resources/**";
	private static final String RESOURCE_LOCATION = "/resources/";
	
	public void ConfigurationDefaultServletHandling(DefaultServletHandlerConfigurer configurer){
		configurer.enable();
	}
	
	@Bean 
	public InternalResourceViewResolver viewResolver(){
		
		InternalResourceViewResolver resolver= new InternalResourceViewResolver();
		resolver.setPrefix(WEBPAGE_PREFIX);
		resolver.setSuffix(WEBPAGE_SUFFIX);
		
		return resolver;
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler(RESOURCE_HANDLER)
                    .addResourceLocations(RESOURCE_LOCATION);
    }
 
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/locale/locale");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}
}
