package com.itinterview.validator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.itinterview.bean.RegisterUserBean;
import com.itinterview.entity.User;
import com.itinterview.service.UserService;
import com.itinterview.validator.core.Result;
import com.itinterview.validator.core.ValidationResult;
import com.itinterview.validator.core.Validator;

@Component
@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class UserValidator extends Validator<RegisterUserBean>{

	private static final Logger LOG= Logger.getLogger(UserValidator.class);
	
	private static final String USERS_LOGIN_EXSIST= "Uzytkownik o takim loginie juz istnieje";
	private static final String USER_EMAIL_EXSIST= "Uzytkownik o takim adresie email juz istnieje";
	
	@Autowired
	private UserService userService;
	
	public UserValidator(){
		super();
	}
	
	private boolean checkIfLoginExist(){
		return (userService.getUserByLogin(objectToValidate.getLogin())==null) ? false : true; 
	}
	
	private boolean checkIfEmailExist(){
		return (userService.getUserByEmail(objectToValidate.getEmail())==null) ? false : true;
	}
	
	@Override
	protected Result performValidation(){
		
		Result result= new Result(ValidationResult.VALID);
		
		if(checkIfLoginExist()){
			result.setResult(ValidationResult.NOT_VALID);
			result.addMessage(USERS_LOGIN_EXSIST);
			LOG.debug(String.format("performValidation : user not valid, login '%s' already exsist", objectToValidate.getLogin()));
		}
		if(checkIfEmailExist()){
			result.setResult(ValidationResult.NOT_VALID);
			result.addMessage(USER_EMAIL_EXSIST);
			LOG.debug(String.format("performValidation : user not valid, email '%s' already exsist", objectToValidate.getEmail()));
		}
		
		return result;
	}
}
