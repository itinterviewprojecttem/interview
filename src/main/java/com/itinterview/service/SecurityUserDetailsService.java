package com.itinterview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.itinterview.entity.User;
import com.itinterview.security.SecurityUser;

@Component
public class SecurityUserDetailsService implements UserDetailsService{
	
	@Autowired
	private UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userService.getUserByLogin(username);
		
		if(user == null){
			System.out.println("User '"+username+"' doesn't exsist");
			throw new UsernameNotFoundException(String.format("User '%s' doesn't exsist", username));
		}
		
		System.out.println("User username '"+username+"' found : %s"+user);
		UserDetails userDetails = new SecurityUser(user);
		
		if(!userDetails.isEnabled()){
			throw new SecurityException("Konto uzytkownika czasowo nieaktywne");
		}
		
		return userDetails;
	}

}
