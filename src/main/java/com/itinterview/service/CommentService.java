package com.itinterview.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.bean.CommentBean;
import com.itinterview.dao.CommentDao;
import com.itinterview.entity.Comment;
import com.itinterview.entity.Company;
import com.itinterview.entity.CompanyComment;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionComment;
import com.itinterview.entity.factory.EntityModelFactory;

@Service
public class CommentService {
	
	@Autowired
	private EntityModelFactory entityModelFactory;
	
	@Autowired
	private CommentDao commentDao;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private QuestionService questionService;
	
	private Comment getComment(CommentBean commentBean){
		Comment comment= entityModelFactory.createComment(commentBean);
		
		return comment;
	}
	
	public Comment persistCommentCompany(CommentBean commentBean, Integer companyId){
		Comment comment= getComment(commentBean);
		Company company= companyService.getById(companyId);
		CompanyComment companyComment= entityModelFactory.createComment(comment, company);
		Set<CompanyComment> companyComments= new HashSet<>();
		companyComments.add(companyComment);
		comment.setCompanyComments(companyComments);
		comment= commentDao.save(comment);
		
		return comment;
	}
	
	public Comment persistCommentQuestion(CommentBean commentBean, Integer questionId){
		Comment comment= getComment(commentBean);
		Question question= questionService.getById(questionId);
		QuestionComment questionComment= entityModelFactory.createQuestionComment(comment, question);
		Set<QuestionComment> questionComments= new HashSet<>();
		questionComments.add(questionComment);
		comment.setQuestionComments(questionComments);
		comment= commentDao.save(comment);
		
		return comment;
	}
	
	public List<Comment> getCommentsForCompany(Integer companyId){
		
		List<Comment> comments= commentDao.findCommentsForCompany(companyId);
		
		return comments;
	}
	
	public List<Comment> getCommentsForQuestion(Integer questionId){
		
		List<Comment> comments= commentDao.findCommentsForQuestion(questionId);
		
		return comments;
	}
	
	public Comment getById(Integer id){
		return commentDao.findOne(id);
	}
	
	public Comment update(CommentBean commentBean){
		Comment commentToUpdate= commentDao.findOne(commentBean.getCommentId());
		commentToUpdate.setText(commentBean.getText());
		commentToUpdate.setUser(commentBean.getAuthor());
		commentToUpdate= commentDao.save(commentToUpdate);
		
		return commentToUpdate;
	}
}
