package com.itinterview.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.method.annotation.ModelFactory;

import com.itinterview.bean.QuestionBean;
import com.itinterview.dao.QuestionDao;
import com.itinterview.entity.Answer;
import com.itinterview.entity.Company;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionAnswer;
import com.itinterview.entity.QuestionCompany;
import com.itinterview.entity.factory.EntityModelFactory;

@Service
public class QuestionService {
	
	@Autowired
	private QuestionDao questionDao;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private EntityModelFactory modelFactory;
	
	public Question getById(Integer id){
		return questionDao.getOne(id);
	}
	
	private Set<QuestionAnswer> getAnswerForQuestion(QuestionBean questionBean, Question question){
		String answerTxt= questionBean.getAnswer();
		Answer answer= modelFactory.createAnswer(answerTxt);
		answer= answerService.persist(answer);
		Set<QuestionAnswer> questionAnswers= new HashSet<>();
		questionAnswers.add(modelFactory.createQuestionAnswer(question, answer));
		
		return questionAnswers;
	}
	
	public Question persist(QuestionBean questionBean){
		
		Question question= modelFactory.createQuestion(questionBean);
		String answerTxt= questionBean.getAnswer();
		
		if((answerTxt!=null) && (!answerTxt.isEmpty())){
			Set<QuestionAnswer> questionAnswers= getAnswerForQuestion(questionBean, question);
			question.setQuestionAnswers(questionAnswers);
		}
		
		Integer companyId= questionBean.getCompanyId();
		Company company= companyService.getById(companyId);
		Set<QuestionCompany> questionCompany= new HashSet<>();
		questionCompany.add(modelFactory.createQuestionCompany(question, company));
		question.setQuestionCompanies(questionCompany);
		
		question= questionDao.save(question);
		
		return question;
	}
	
	public List<Question> getLast(int count){
		Pageable pageable= new PageRequest(0, 10);
		return questionDao.findByOrderByDateDesc(pageable);
	}
}
