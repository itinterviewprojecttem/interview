package com.itinterview.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.dao.TechnologyDao;
import com.itinterview.entity.Technology;

@Service
public class TechnologyService {
	
	@Autowired
	private TechnologyDao technologyDao;
	
	public Technology persist(String value){
		
		Technology technology= technologyDao.findOneByValue(value);
		
		if(technology!=null){
			return technology;
		}
		
		technology= new Technology();
		technology.setValue(value);
		technology= technologyDao.save(technology);
		
		return technology;
	}
	
	public List<Technology> persist(List<String> technologies){
		
		List<Technology> persisted= new ArrayList<>(); 
		
		for(String tech : technologies){
			persisted.add(persist(tech));
		}
		
		return persisted;
	}
	
	public Technology getByName(String name){
		return technologyDao.findOneByValue(name);
	}
}
