package com.itinterview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.dao.UserDao;
import com.itinterview.dao.UserTypeDao;
import com.itinterview.entity.UserType;

@Service
public class UserTypeService {
	
	@Autowired
	private UserTypeDao userTypeRepository;
	
	public UserType getUserType(String type){
		return userTypeRepository.findOneByType(type);
	}
}
