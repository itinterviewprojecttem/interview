/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.service;

import com.itinterview.bean.RegisterUserBean;
import com.itinterview.dao.UserDao;
import com.itinterview.dao.UserUserTypeDao;
import com.itinterview.entity.User;
import com.itinterview.entity.UserUserType;
import com.itinterview.entity.enums.UserTypes;
import com.itinterview.entity.factory.EntityModelFactory;

import java.io.Serializable;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author kurekk
 */

@Service("userService")
public class UserService {
    
    private static final Logger LOGGER = Logger.getLogger(UserService.class);
    
    @Autowired
    private UserDao userRepository;
    
    @Autowired
    private UserUserTypeService userUserTypeService;
    
    @Autowired
    private EntityModelFactory entityModelFactory;
    
    public User persistUser(RegisterUserBean registerUserBean, UserTypes userTypes){
        
        User user= entityModelFactory.createUserModel(registerUserBean);
        user= userRepository.save(user);
        UserUserType userUserType= entityModelFactory.createUserUserType(user, userTypes);
      
        userUserTypeService.persist(userUserType);
        
        return user;
    }
    
    public User getUserByLogin(String login){
    	return userRepository.findOneByLogin(login);
    }

    public User getUserByEmail(String email){
    	return userRepository.findOneByEmail(email);
    }
    
    public UserDao getUserRepository() {
        return userRepository;
    }
    
    public void setUserRepository(UserDao userRepository) {
        this.userRepository = userRepository;
    }
    
    public List<User> getAll(){
    	return userRepository.findAll();
    }
    
    public User update(Integer userId, Integer status){
    	
    	User user= userRepository.getOne(userId);
    	user.setStatus(status);
    	user= userRepository.save(user);
    	
    	return user;
    }
}
