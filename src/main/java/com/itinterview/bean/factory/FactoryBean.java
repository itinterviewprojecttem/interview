package com.itinterview.bean.factory;

import java.util.Date;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.itinterview.bean.AnswerBean;
import com.itinterview.bean.CommentBean;
import com.itinterview.bean.CompanyBean;
import com.itinterview.entity.Answer;
import com.itinterview.entity.Comment;
import com.itinterview.entity.Company;
import com.itinterview.entity.CompanyTechnolgies;
import com.itinterview.entity.User;

@Component
public class FactoryBean {
	
	private String getTechnologies(Company company){
		String technologies= "";
		
		Set<CompanyTechnolgies> companyTechnologies= company.getCompanyTechnolgieses();
		for(CompanyTechnolgies companyTech : companyTechnologies){
			String technology= companyTech.getTechnology().getValue();
			if(technology==null){
				continue;
			}
			technologies= (technologies.length() > 0) ? (technologies.concat(CompanyBean.TECHNOLOGY_DELIM).concat(technology)) 
					: (technologies.concat(technology));
		}
		
		return technologies;
	}
	
	public CompanyBean getCompanyBean(Company company){
		CompanyBean companyBean= new CompanyBean();
		companyBean.setAuthor(company.getUser());
		companyBean.setDescription(company.getDescription());
		companyBean.setId(company.getId());
		companyBean.setName(company.getName());
		companyBean.setTechnologies(getTechnologies(company));
		companyBean.setWebsite(company.getWebsite());
		
		return companyBean;
	}
	
	public CommentBean getCommentBean(Comment comment, User user){
		CommentBean commentBean= new CommentBean();
		commentBean.setAuthor(user);
		commentBean.setDate(new Date());
		commentBean.setText(comment.getText());
		commentBean.setCommentId(comment.getId());
		
		return commentBean;
	}
	
	public AnswerBean getAnswerBean(Answer answer, Integer questionId){
		AnswerBean answerBean= new AnswerBean();
		answerBean.setText(answer.getText());
		answerBean.setQuestionId(questionId);
		answerBean.setAnswerId(answer.getId());
		
		return answerBean;
	}
}
