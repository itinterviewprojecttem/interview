package com.itinterview.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.itinterview.entity.User;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CompanyBean {

	public static final String TECHNOLOGY_DELIM = ",";
	
	private Integer id;

	@NotNull
	@Size(min = 1, message = "Input company name")
	private String name;
	private String website;
	private String description;
	private String technologies;
	private User author;
	private boolean edit;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}
	
	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public String getTechnologies() {
		return technologies;
	}

	public List<String> getTechnolgiesAsList(){
		
		List<String> techsList= new ArrayList<>();
		
		if((technologies==null) || (technologies.isEmpty())){
			return techsList;
		}
		
		String techsArray[]= technologies.split(TECHNOLOGY_DELIM);
		for(String techName : techsArray){
			if(techName.isEmpty()){
				continue;
			}
			techsList.add(techName);
		}
		
		return techsList;
	}
	
	public void setTechnologies(String technologies) {
		this.technologies = technologies;
	}
}
