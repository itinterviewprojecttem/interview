package com.itinterview.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.itinterview.entity.User;
import com.itinterview.entity.UserType;
import com.itinterview.entity.UserUserType;

class GrantedAuthorityFactory {

	public Collection<? extends GrantedAuthority> getAuthorities(User user) {

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		Set<UserUserType> userTypes = user.getUserUserTypes();
		
		for(UserUserType usrType : userTypes){
			UserType type= usrType.getUserType();
			SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(type.getType());
			authorities.add(grantedAuthority);
		}

		return authorities;
	}
}
