package com.itinterview.controller.enums;

public enum HtmlAttributes {
	
	MSG_ERR("errorMessage"),
	MSG_SUCC("actionStatus"),
	ATTR_ID_CPMANY("companyId"),
	ATTR_ID_QUESTION("questionId"),
	ATTR_EDIT("edit");
	
	private String attribute;
	
	private HtmlAttributes(String attribute) {
		this.attribute= attribute;
	}

	public String getAttribute() {
		return attribute;
	}
}
