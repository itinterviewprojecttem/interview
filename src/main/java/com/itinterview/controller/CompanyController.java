/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.controller;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.itinterview.bean.CommentBean;
import com.itinterview.bean.CompanyBean;
import com.itinterview.bean.factory.FactoryBean;
import com.itinterview.controller.enums.HtmlAttributes;
import com.itinterview.entity.Comment;
import com.itinterview.entity.Company;
import com.itinterview.entity.User;
import com.itinterview.service.CommentService;
import com.itinterview.service.CompanyService;
import com.itinterview.validator.CompanyValidator;
import com.itinterview.validator.core.ValidationResult;
import com.itinterview.security.SecurityManager;
import com.itinterview.security.SecurityUser;

/**
 *
 * @author kurekk
 */

@Controller
@RequestMapping("company")
public class CompanyController {
    
    private static final Logger LOGGER = Logger.getLogger(CompanyController.class);
    
    @Autowired
    private CompanyService companyService;
    @Autowired 
    private CommentService commentService;
    @Autowired
    private CompanyBean companyBean;
    @Autowired
    private CommentBean commentBean;
    @Autowired
    private SecurityManager securityManager;
    @Autowired
    private CompanyValidator companyValidator;
    @Autowired
    private FactoryBean factoryBean;
    
    @RequestMapping(method = RequestMethod.GET, value = "/search")
    public String searchGet(HttpServletRequest req, @RequestParam(value="letter", required=false)String letter, 
    		@ModelAttribute(value="companyName") String companyName){

    	List<Company> companies= new LinkedList<>();
    	req.setAttribute("companies", companies);
    	
    	if((companyName!=null) && (!companyName.isEmpty())){
    		companies.addAll(companyService.getContainingName(companyName));
    		return "/company/search";
    	}
    	if((letter!=null) && (!letter.isEmpty())){
    		companies.addAll(companyService.getCompaniesNameStartsWith(letter));
    		return "/company/search";
    	}
    	
    	companies.addAll(companyService.getAllComapny());
    	
    	return "/company/search";
    }
    
    @RequestMapping(method = RequestMethod.GET, value="/add")
    public String addGet(){
        return "company/add";
    }
    
    @RequestMapping(method= RequestMethod.POST, value="/add")
    public String addPost(@ModelAttribute("companyBean")@Validated CompanyBean companyBean,
    		BindingResult bindingResult, HttpServletRequest request){
    	
    	if(bindingResult.hasErrors()){
    		request.setAttribute(HtmlAttributes.MSG_ERR.getAttribute(), "Blad w formularzu");
    		return "company/add";
    	}
    	
    	if(companyValidator.isValid(companyBean).getResult().equals(ValidationResult.NOT_VALID)){
    		String errMSg= companyValidator.getResult().getMessageAsString();
    		request.setAttribute(HtmlAttributes.MSG_ERR.getAttribute(), errMSg);
    		return "company/add";
    	}
    	
    	SecurityUser user= securityManager.getCurrentUser();
    	companyBean.setAuthor(user.getUser());
    	companyService.persist(companyBean);
    	request.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Dodano nowa firme");
    	
    	return "company/add";
    }
    
    @RequestMapping(method = RequestMethod.GET, value="/details/{id}")
    public String displayCompanyDetails(HttpServletRequest req, @PathVariable Integer id){
        Company company = companyService.getById(id);
        req.setAttribute("company", company);
        
        List<Comment> comments= commentService.getCommentsForCompany(id);
        req.setAttribute("comments", comments);
        
        return "company/details";
    }
    
    @RequestMapping(method=RequestMethod.GET, value="/edit/{id}")
    public String editGet(HttpServletRequest req, @PathVariable Integer id){
    	
    	Company company= companyService.getById(id);
    	CompanyBean companyBean= factoryBean.getCompanyBean(company);
    	req.setAttribute("companyBean", companyBean);
    	req.setAttribute(HtmlAttributes.ATTR_EDIT.getAttribute(), true);
    	
    	return "company/add";
    }
    
    @RequestMapping(method= RequestMethod.POST, value="/comment/add/{id}")
    public String addComment(@ModelAttribute("commentBean")@Validated CommentBean commentBean, 
    		BindingResult bindingResult, @PathVariable Integer id){

    	if(bindingResult.hasErrors()){
    		return "redirect:company/details/"+id;
    	}
    	
    	commentBean.setAuthor(securityManager.getCurrentUser().getUser());
    	commentBean.setDate(new Date());
    	commentService.persistCommentCompany(commentBean, id);
    	
    	return "redirect:/company/details/"+id;
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/edit")
    public String editCompanyPost(@ModelAttribute("companyBean")@Validated CompanyBean companyBean,
    		BindingResult bindingResult, HttpServletRequest req){
    	
    	if(bindingResult.hasErrors()){
    		return "company/add";
    	}
    	companyService.update(companyBean);
    	req.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Dane firmy zostaly zaktualizowane");
    	
    	return "company/add";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/comment/edit")
    public String editCommentPost(HttpServletRequest req, RedirectAttributes redirectAttributes){
    	
    	Integer commentId= Integer.parseInt(req.getParameter("commentId"));
    	Integer companyId= Integer.parseInt(req.getParameter("companyId"));
    	
    	Comment commentToEdit= commentService.getById(commentId);
    	User commentAuthor= securityManager.getCurrentUser().getUser();
    	CommentBean commentBean= factoryBean.getCommentBean(commentToEdit, commentAuthor);

    	redirectAttributes.addFlashAttribute("commentBean", commentBean);
    	redirectAttributes.addFlashAttribute("edit", true);
    	
    	return "redirect:/company/details/"+companyId;
    }
    
    @RequestMapping(method=RequestMethod.POST, value="comment/edit/save/{companyId}")
    public String editCommentSavePost(@ModelAttribute("commentBean")@Validated CommentBean commentBean, 
    		BindingResult bindingResult, @PathVariable Integer companyId, HttpServletRequest req){
    	commentBean.setAuthor(securityManager.getCurrentUser().getUser());
    	commentService.update(commentBean);
    	req.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Komentarz zostal zaktualizowany");
    	
    	return "redirect:/company/details/"+companyId;
    }
}
