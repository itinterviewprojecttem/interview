/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itinterview.entity.Company;
import com.itinterview.entity.Question;
import com.itinterview.service.CompanyService;
import com.itinterview.service.QuestionService;

/**
 *
 * @author user
 */

@Controller
public class HomeController {
	
	private static final int QUESTIONS_COUNT = 10;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired 
	private QuestionService questionService;
	
    @RequestMapping(value = "/", method= RequestMethod.GET)
    public String homeGet(HttpServletRequest req){
    	
    	List<Question> questions= questionService.getLast(QUESTIONS_COUNT);
    	req.setAttribute("questions", questions);
    	
    	List<Company> companies= companyService.getLast(10);
    	req.setAttribute("companies", companies);
    	
        return "index";
    }
}
