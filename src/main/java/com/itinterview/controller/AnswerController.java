package com.itinterview.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itinterview.bean.AnswerBean;
import com.itinterview.bean.factory.FactoryBean;
import com.itinterview.controller.enums.HtmlAttributes;
import com.itinterview.entity.Answer;
import com.itinterview.service.AnswerService;
import java.util.Date;

@Controller
@RequestMapping("answer")
public class AnswerController {
	
	private static final Logger LOG= Logger.getLogger(AnswerController.class);
	
	@Autowired
	private AnswerBean answerBean;
	@Autowired
	private AnswerService answerService;
	@Autowired 
	private FactoryBean factoryBean;
	
	@RequestMapping(value="add/{id}", method=RequestMethod.GET)
	public String addAnswerGet(@PathVariable Integer id, HttpServletRequest req){
		
		req.setAttribute(HtmlAttributes.ATTR_ID_QUESTION.getAttribute(), id);
		
		return "answer/add";
	}
	
	@RequestMapping(value="add/{id}", method=RequestMethod.POST)
	public String addAnswerPost(@ModelAttribute("answerBean")@Validated AnswerBean answerBean,
			BindingResult bindingResult, @PathVariable Integer id, HttpServletRequest req){
		
		if(bindingResult.hasErrors()){
			req.setAttribute(HtmlAttributes.MSG_ERR.getAttribute(), "Blad w formularzu");
    		return "answer/add/";
		}
		
		answerBean.setQuestionId(id);
		answerBean.setDate(new Date());
		answerService.persist(answerBean);
		req.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Dodano nowe pytanie");
		
		return "answer/add";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="edit")
    public String editAnswerPost(HttpServletRequest req){
    	Integer answerId= Integer.parseInt(req.getParameter("answerId"));
    	Answer answer= answerService.getById(answerId);
    	Integer questionId= Integer.parseInt(req.getParameter("questionId"));
    	
    	req.setAttribute("edit", true);
    	AnswerBean answerBean= factoryBean.getAnswerBean(answer, questionId);
    	req.setAttribute("answerBean", answerBean);
    	
    	return "/answer/add";
    }
	
	@RequestMapping(value="edit/save/{questionId}/{answerId}", method=RequestMethod.POST)
	public String answerEditPost(@ModelAttribute("answerBean")@Validated AnswerBean answerBean,
			BindingResult bindingResult, @PathVariable Integer questionId, @PathVariable Integer answerId,
			HttpServletRequest req){
		
		if(bindingResult.hasErrors()){
			req.setAttribute(HtmlAttributes.MSG_ERR.getAttribute(), "Edycja nieudana");
			return "/answer/add";
		}
		
		answerBean.setAnswerId(answerId);
		answerBean.setQuestionId(questionId);
		answerService.update(answerBean);
		
		req.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Odpowiedz zostala zaktualizowana");
		
		return "/answer/add";
	}
}
