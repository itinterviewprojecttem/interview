package com.itinterview.dao;

import java.util.List;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.itinterview.entity.Company;

public interface CompanyDao extends JpaRepository<Company, Integer>{
	
	Company findOneByName(String name);
	
	List<Company> findAllByOrderByNameAsc();
	
	@Query("SELECT c FROM Company c WHERE c.name LIKE :name ORDER BY c.name ASC")
	List<Company> searchWithJPQLQuery(@Param("name")String name);
	
	List<Company> findByNameContainingIgnoreCase(String name);
}
