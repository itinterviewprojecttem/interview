package com.itinterview.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itinterview.entity.Question;

public interface QuestionDao extends JpaRepository<Question, Integer>{
	List<Question> findAllByOrderByDateDesc();
	List<Question> findByOrderByDateDesc(Pageable pageable);
}
