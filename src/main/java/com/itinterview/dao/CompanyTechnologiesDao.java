package com.itinterview.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.itinterview.entity.CompanyTechnolgies;

public interface CompanyTechnologiesDao extends JpaRepository<CompanyTechnolgies, Integer>{
	List<CompanyTechnolgies> findByCompanyNameAndTechnologyValue(String companyName, String technologyValue);
	@Modifying
	@Transactional
	@Query(value="delete from CompanyTechnolgies t where t.id=?1")
	void deleteById(Integer id);
}
