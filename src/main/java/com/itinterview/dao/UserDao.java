/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.dao;

import com.itinterview.entity.User;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

/**
 *
 * @author kurekk
 */
public interface UserDao extends JpaRepository<User, Integer>{
	User findOneByLogin(String login);
	User findOneByEmail(String email);
}
