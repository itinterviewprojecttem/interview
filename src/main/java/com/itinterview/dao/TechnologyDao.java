package com.itinterview.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itinterview.entity.Technology;

public interface TechnologyDao extends JpaRepository<Technology, Integer>{
	Technology findOneByValue(String value);
}
