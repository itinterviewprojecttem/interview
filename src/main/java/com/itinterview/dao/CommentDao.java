package com.itinterview.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.itinterview.entity.Comment;

public interface CommentDao extends JpaRepository<Comment, Integer>{
	
	@Query("SELECT DISTINCT comment FROM Comment comment "
			+ "join comment.companyComments compComms WHERE "
			+ "compComms.company.id= :id ORDER BY compComms.comment.createddate")
	List<Comment> findCommentsForCompany(@Param("id")Integer id);
	
	@Query("SELECT DISTINCT comment FROM Comment comment "
			+ "join comment.questionComments questComms WHERE "
			+ "questComms.question.id= :id ORDER BY questComms.comment.createddate")
	List<Comment> findCommentsForQuestion(@Param("id")Integer id);
}
