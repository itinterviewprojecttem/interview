package com.itinterview.entity;
// Generated Mar 18, 2016 10:13:14 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * User generated by hbm2java
 */
@Entity
@Table(name = "user")
public class User implements java.io.Serializable {

	private Integer id;
	private String email;
	private String login;
	private String password;
	private Integer status;
	private Date createddate;
	private Set<UserUserType> userUserTypes = new HashSet<UserUserType>(0);
	private Set<Comment> comments = new HashSet<Comment>(0);
	private Set<Company> companies = new HashSet<Company>(0);
	private Set<Question> questions = new HashSet<Question>(0);

	public User() {
	}

	public User(String email) {
		this.email = email;
	}

	public User(String email, String login, String password, Integer status, Set<UserUserType> userUserTypes,
			Set<Comment> comments, Set<Company> companies, Set<Question> questions) {
		this.email = email;
		this.login = login;
		this.password = password;
		this.status = status;
		this.userUserTypes = userUserTypes;
		this.comments = comments;
		this.companies = companies;
		this.questions = questions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "email", nullable = false, length = 128)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "login", length = 32)
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "password", length = 256)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Column(name="createddate")
	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	public Set<UserUserType> getUserUserTypes() {
		return this.userUserTypes;
	}

	public void setUserUserTypes(Set<UserUserType> userUserTypes) {
		this.userUserTypes = userUserTypes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Company> getCompanies() {
		return this.companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Question> getQuestions() {
		return this.questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

}
