package com.itinterview.entity;
// Generated Mar 18, 2016 10:13:14 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Question generated by hbm2java
 */
@Entity
@Table(name = "question")
public class Question implements java.io.Serializable {

	private Integer id;
	private User user;
	private String text;
	private Date date;
	private Set<QuestionCompany> questionCompanies = new HashSet<QuestionCompany>(0);
	private Set<QuestionComment> questionComments = new HashSet<QuestionComment>(0);
	private Set<QuestionJob> questionJobs = new HashSet<QuestionJob>(0);
	private Set<QuestionAnswer> questionAnswers = new HashSet<QuestionAnswer>(0);

	public Question() {
	}

	public Question(String text) {
		this.text = text;
	}

	public Question(User user, String text, Set<QuestionCompany> questionCompanies,
			Set<QuestionComment> questionComments, Set<QuestionJob> questionJobs, Set<QuestionAnswer> questionAnswers) {
		this.user = user;
		this.text = text;
		this.questionCompanies = questionCompanies;
		this.questionComments = questionComments;
		this.questionJobs = questionJobs;
		this.questionAnswers = questionAnswers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_author")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "text", nullable = false)
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Column(name="createddate")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "question", cascade= CascadeType.PERSIST)
	public Set<QuestionCompany> getQuestionCompanies() {
		return this.questionCompanies;
	}

	public void setQuestionCompanies(Set<QuestionCompany> questionCompanies) {
		this.questionCompanies = questionCompanies;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "question")
	public Set<QuestionComment> getQuestionComments() {
		return this.questionComments;
	}

	public void setQuestionComments(Set<QuestionComment> questionComments) {
		this.questionComments = questionComments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "question")
	public Set<QuestionJob> getQuestionJobs() {
		return this.questionJobs;
	}

	public void setQuestionJobs(Set<QuestionJob> questionJobs) {
		this.questionJobs = questionJobs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "question", cascade= CascadeType.PERSIST)
	public Set<QuestionAnswer> getQuestionAnswers() {
		return this.questionAnswers;
	}

	public void setQuestionAnswers(Set<QuestionAnswer> questionAnswers) {
		this.questionAnswers = questionAnswers;
	}

}
