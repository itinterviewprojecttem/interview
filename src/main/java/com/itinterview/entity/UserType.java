package com.itinterview.entity;
// Generated Mar 18, 2016 10:13:14 PM by Hibernate Tools 4.3.1.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * UserType generated by hbm2java
 */
@Entity
@Table(name = "user_type", uniqueConstraints = @UniqueConstraint(columnNames = "type"))
public class UserType implements java.io.Serializable {

	private Integer id;
	private String type;
	private Set<UserUserType> userUserTypes = new HashSet<UserUserType>(0);

	public UserType() {
	}

	public UserType(String type, Set<UserUserType> userUserTypes) {
		this.type = type;
		this.userUserTypes = userUserTypes;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type", unique = true, length = 32)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userType")
	public Set<UserUserType> getUserUserTypes() {
		return this.userUserTypes;
	}

	public void setUserUserTypes(Set<UserUserType> userUserTypes) {
		this.userUserTypes = userUserTypes;
	}

}
