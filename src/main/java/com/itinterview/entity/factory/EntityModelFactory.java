/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.entity.factory;

import com.itinterview.bean.AnswerBean;
import com.itinterview.bean.CommentBean;
import com.itinterview.bean.CompanyBean;
import com.itinterview.bean.QuestionBean;
import com.itinterview.bean.RegisterUserBean;
import com.itinterview.dao.UserTypeDao;
import com.itinterview.entity.Answer;
import com.itinterview.entity.Comment;
import com.itinterview.entity.Company;
import com.itinterview.entity.CompanyComment;
import com.itinterview.entity.CompanyTechnolgies;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionAnswer;
import com.itinterview.entity.QuestionComment;
import com.itinterview.entity.QuestionCompany;
import com.itinterview.entity.Technology;
import com.itinterview.entity.User;
import com.itinterview.entity.UserType;
import com.itinterview.entity.UserUserType;
import com.itinterview.entity.enums.UserStatus;
import com.itinterview.entity.enums.UserTypes;
import com.itinterview.service.CompanyTechnologiesService;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kurekk
 */

@Component
public class EntityModelFactory {
    
    @Autowired
    private UserTypeDao userTypeRepo;
    
    private static final UserStatus DEFAULT_USR_STATUS = UserStatus.NOT_ACTIVATED;
    
    public User createUserModel(RegisterUserBean regUsrBean){
        
        User user= new User();
        user.setEmail(regUsrBean.getEmail());
        user.setLogin(regUsrBean.getLogin());
        user.setStatus(DEFAULT_USR_STATUS.getStatus());
        user.setPassword(regUsrBean.getPass());
        user.setCreateddate(new Date());
        
        return user;
    }
    
    public UserUserType createUserUserType(User user, UserTypes userType){
    	
    	UserType usrTypeName = userTypeRepo.findOneByType(userType.getType());
        UserUserType userUserType = new UserUserType();
        userUserType.setUser(user);
        userUserType.setUserType(usrTypeName);
        
        return userUserType;
    }
    
    public Company createCompany(CompanyBean companyBean){
    	
    	Company company= new Company();
    	company.setDescription(companyBean.getDescription());
    	company.setName(companyBean.getName());
    	company.setWebsite(companyBean.getWebsite());
    	company.setUser(companyBean.getAuthor());
    	company.setCreateddate(new Date());
    	
    	return company;
    }
    
    public Company updateCompany(Company company, CompanyBean companyBean){
    	company.setDescription(companyBean.getDescription());
    	company.setName(companyBean.getName());
    	company.setWebsite(companyBean.getWebsite());
    	
    	return company;
    }
    
    public Technology createTechnology(String technology){

    	Technology techModel= new Technology();
    	techModel.setValue(technology);
    	return techModel;
    }
    
    public CompanyTechnolgies createCompanyTechnology(Company company, Technology technology){
    	CompanyTechnolgies companyTechnologies= new CompanyTechnolgies();
    	companyTechnologies.setCompany(company);
    	companyTechnologies.setTechnology(technology);
    	
    	return companyTechnologies;
    }
    
    public Answer createAnswer(String answerText){
    	Answer answer= new Answer();
    	answer.setText(answerText);
    	answer.setCreateddate(new Date());
    
    	return answer;
    }
    
    public Question createQuestion(QuestionBean questionBean){
    	
    	Question question= new Question();
    	question.setText(questionBean.getText());
    	question.setUser(questionBean.getAuthor());
    	question.setDate(new Date());
    	
    	return question;
    }
    
    public QuestionAnswer createQuestionAnswer(Question question, Answer answer){
    	QuestionAnswer questionAnswer= new QuestionAnswer();
    	questionAnswer.setAnswer(answer);
    	questionAnswer.setQuestion(question);
    	
    	return questionAnswer;
    }
    
    public QuestionCompany createQuestionCompany(Question question, Company company){
    	
    	QuestionCompany questionCompany= new QuestionCompany();
    	questionCompany.setCompany(company);
    	questionCompany.setQuestion(question);
    	
    	return questionCompany;
    }
    
    public Answer createAnswer(AnswerBean answerBean){
    	Answer answer= new Answer();
    	answer.setText(answerBean.getText());
    	answer.setCreateddate(answerBean.getDate());
    	
    	return answer;
    }
    
    public Comment createComment(CommentBean commentBean){
    	Comment comment= new Comment();
    	comment.setCreateddate(commentBean.getDate());
    	comment.setText(commentBean.getText());
    	comment.setUser(commentBean.getAuthor());
    	
    	return comment;
    }
    
    public CompanyComment createComment(Comment comment, Company company){
    	CompanyComment companyComment= new CompanyComment();
    	companyComment.setComment(comment);
    	companyComment.setCompany(company);
    	
    	return companyComment;
    }
    
    public QuestionComment createQuestionComment(Comment comment, Question question){
    	QuestionComment questionComment= new QuestionComment();
    	questionComment.setComment(comment);
    	questionComment.setQuestion(question);
    	
    	return questionComment;
    }
}
