/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.entity.enums;

/**
 *
 * @author kurekk
 */
public enum UserTypes {
    ADMIN("ROLE_ADMIN"),
    BASIC("ROLE_USER");
    
    private UserTypes(String type){
        this.type= type;
    }
    
    private String type;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
}
