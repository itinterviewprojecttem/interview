/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.entity.enums;

/**
 *
 * @author kurekk
 */
public enum UserStatus {
    NOT_ACTIVATED(0),
    ACTIVATED(1),
    DISACTIVATED(2);
    
    private int status;

    private UserStatus(int status) {
        this.status = status;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }
    
    public static UserStatus getByValue(int userStatus){
    	for(UserStatus status : UserStatus.values()){
    		if(status.getStatus()== userStatus){
    			return status;
    		}
    	}
    	
    	return null;
    }
}
