<%@tag language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="content" fragment="true"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; utf-8">
        
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script
        src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-override.css" />" >
        <link rel="stylesheet" href="<c:url value="/resources/css/main.css" />">
        <link rel="stylesheet" href="<c:url value="/resources/css/leftmenu.css" />">
        
        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-tagsinput.css" />">
        <script src="<c:url value="/resources/js/bootstrap-tagsinput.js" />"></script>
        <script src="<c:url value="/resources/js/bootstrap-tagsinput-angular.js" />" ></script>
        <script src="<c:url value="/resources/js/modal/modals.js" />" ></script>
        <title>ITInterview</title>
    </head>
    <body>
    	<script>showModals('${loginModal}');</script>
        <div class="row">
        <nav class="navbar navbar-default clear">
            <div class="col-md-2"></div>
            <div class="col-md-8 clear">
                    <div class="container-fluid">
                        <div>
                            <ul class="nav navbar-nav">
                                <li> <a href="${pageContext.request.contextPath}/">Home</a></li>
                                <li><a href="${pageContext.request.contextPath}/company/search">Wyszukaj Firme</a></li>
                                <sec:authorize access="hasRole('ROLE_USER')">
                                	<li><a href="${pageContext.request.contextPath}/company/add">Dodaj Firme</a></li>
                                </sec:authorize>
                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                	<spring:url value="/user/list" var="usersUrl"/>
                                	<li><a href="${fn:escapeXml(usersUrl)}">Użytkownicy</a></li>
                                </sec:authorize>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
                                    <li><a href="${pageContext.request.contextPath}/user/register"><span class="glyphicon glyphicon-user"></span>Zarejestruj</a></li>
                                    <li><!--<a href="${pageContext.request.contextPath}/user/login"><span class="glyphicon glyphicon-log-in"></span>Zaloguj</a>-->
                                    	<a href="#" data-toggle="modal" data-target="#loginModal"><span class="glyphicon glyphicon-log-in"></span>Zaloguj</a>
                                    </li>
                                    <!-- Modal -->
                                    <t:custommodal>
										<jsp:attribute name="title">
											Formularz Logowania
										</jsp:attribute>
										<jsp:attribute name="body">
											<form id="login-form" name="loginForm" action="${pageContext.request.contextPath}/user/login"
													method="post">
										    	<div class="form-group">
										        	<label for="login">Login</label>
										            <input name="username" type="text" class="form-control" id="username" />
										        </div>
										        <div class="form-group">
										        	<label for="pass">Hasło</label>
										            <input name="password" type="password" class="form-control" id="password" />
										        </div>
										        <div class="submit-btn-container">
										        	<button type="submit" class="btn btn-default">
														<strong>Zaloguj</strong>
													</button>
										        </div>
										        <c:if test="${login_error}">
										        	<label class="alert-danger">
										        		<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
										        	</label>
										       </c:if>
											</form>
										</jsp:attribute>  
										<jsp:attribute name="closeBtnLabel"></jsp:attribute>                                  
                                    </t:custommodal>
                                    <!-- End Modal -->
                                </sec:authorize> 
                                <sec:authorize access="isAuthenticated()">
                                	 <li><a href="${pageContext.request.contextPath}/user/logout">
                                	 	<span class="glyphicon glyphicon-user"></span>Logout(<sec:authentication property="principal.username"/>)</a>
                                	 </li>
                                </sec:authorize>
                            </ul>
                        </div>
                    </div>
                   </div>
                </nav>
            </div>
    <div class="row">
        <div id="left-menu" class="col-md-2">
            <!--<ul class="list-group">
                <li class="list-group-item active">Companies List</li>
                <li class="list-group-item"><a href="#">Company Two</a></li>
                <li class="list-group-item"><a href="#">Company Three</a></li>
            </ul>-->
        </div>
        <div class="col-md-8 clear">
            <div class="content">
                <jsp:invoke fragment="content" />
            </div>
        </div>
    </div>
    <div id="idFooter" class="row">
            <div class="col-md-12 well well-lg">
                <div class="col-md-2"></div>
                <div class="col-md-2">
                	<a href="http://www.pracuj.pl/">
                		<img src="<c:url value="/resources/img/logo-pracuj-pl.svg" />">
                	</a>
                </div>
                <div class="col-md-2">
                	<a href="https://www.linkedin.com">
     					<img src="<c:url value="/resources/img/logo-linkedin.png" />"/>
                	</a>
                </div>
                <div class="col-md-2">
                	<a href="https://nofluffjobs.com">
                		<img src="<c:url value="/resources/img/logo-nfj.png"/>" />
                	</a>
                </div>
                <div class="col-md-2">
                	<a href="http://www.goldenline.pl/">
                		<img src="<c:url value="/resources/img/logo-goldenline.jpg" />" />
                	</a>
                </div>
            </div>
     </div>
</body>
</html>
