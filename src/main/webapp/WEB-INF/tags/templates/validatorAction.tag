<%@ tag language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; utf-8">
</head>
<body>
	<c:if test="${errorMessage!=null}">
  		<div class="alert alert-danger">
  			<strong><c:out value="${errorMessage}" /></strong>
  		</div>
  	</c:if>
  	<c:if test="${actionStatus!=null}">
  		<div class="alert alert-success">
  			<strong><c:out value="${actionStatus}"></c:out></strong>
  		</div>
  	</c:if>
</body>
</html>