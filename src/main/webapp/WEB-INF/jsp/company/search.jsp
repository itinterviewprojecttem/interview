<%@ page language="java" contentType="text/html; utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<t:pagetemplate>
    <jsp:attribute name="content">
        <div class="search-title-container">
            <div class="search-title">Wyszukaj Firme</div>
        </div>
        <spring:url value="/company/search" var="actionUrl"/>
        <form id="search-company" class="navbar-form" method="get" action="${actionUrl}">
            <div class="input-group search-input-group">
                <input type="text" class="form-control" name="companyName">
                <div id="search-input-group-btn" class="input-group-btn">
                    <button class="btn btn-default " type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form> 
        <div>
        	<div class="search-result-container">
        		Znaleziono <span><c:out value="${companies.size()}"/></span> wyników
        	</div>
        	<div class="search-filter-letter">
        		<ul>
        			<c:set var="alphabet" value="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z" />
					<c:forTokens var="letter" items="${alphabet}" delims=",">
        				<li><a href="${pageContext.request.contextPath}/company/search?letter=${letter}"><c:out value="${letter}" /></a></li>
        			</c:forTokens>
        		</ul>
        	</div>
        	<ul class="list-group">
        		<c:forEach var="company" items="${companies}">
        			<li class="list-group-item">
        				<a href="${pageContext.request.contextPath}/company/details/${company.id}"><c:out value="${company.name}" /></a>
        			</li>	
        		</c:forEach>
        	</ul>
        </div>
    </jsp:attribute>
</t:pagetemplate>