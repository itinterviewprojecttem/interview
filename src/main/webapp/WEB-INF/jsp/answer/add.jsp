<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="answerBean" scope="request" class="com.itinterview.bean.AnswerBean" />

<head>
<script src="<c:url value="/resources/js/tag/tags.js" />"></script>
</head>

<t:pagetemplate>
	<jsp:attribute name="content">
        <div class="search-title-container">
            <div id="add-answer" class="search-title">Dodaj Odpowiedź</div>
        </div>
        <t:validatorAction />
        <c:if test="${edit==true}">
        	<spring:url value="/answer/edit/save/${answerBean.questionId}/${answerBean.answerId}" var="actionUrl"/>
        </c:if>
        <c:if test="${edit!=true}">
        	<spring:url value="/answer/add/${questionId}" var="actionUrl"/>
        </c:if>
        <form:form method="post" role="form" commandName="answerBean" action="${actionUrl}">
            <div class="form-group">
                <label for="companyDesc">Treść odpowiedzi</label>
                <form:textarea path="text" class="form-control" rows="12"></form:textarea>
                <div><label class="alert-danger"><form:errors path="text"/></label></div>
            </div>
            <div class="submit-btn-container">
                <input type="submit" class="btn btn-default" value="Zapisz"/>
            </div>
    	</form:form>
	</jsp:attribute>
</t:pagetemplate>