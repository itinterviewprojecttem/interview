<%@ page language="java" contentType="text/html; utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="registerUserBean" scope="request" class="com.itinterview.bean.RegisterUserBean" />

<t:pagetemplate>
	<jsp:attribute name="content">
            <h2>Formularz Rejestracyjny</h2>
            <t:validatorAction />
            <form:form method="post" role="form" commandName="registerUserBean" action="${pageContext.request.contextPath}/user/register">
                <div class="form-group">
                    <label  for="email">Email</label>
                    <form:input type="email" class="form-control" id="email" path="email"/>
                    <div class="alert-danger"><strong><form:errors path="email" /></strong></div>
                </div>
                <div class="form-group">
                    <label  for="email-rep">Powtórz Email</label>
                    <form:input type="email" class="form-control" id="rep-email" path="repEmail"/>
                    <div class="alert-danger"><strong><form:errors path="repEmail"/></strong></div>
                </div>
                <div class="form-group">
                    <label for="login">Login</label>
                    <form:input type="text" class="form-control" id="login" path="login"/>
                    <div class="alert-danger"><strong><form:errors path="login"/></strong></div>
                </div>
                <div class="form-group">
                    <label for="pass">Hasło</label>
                    <form:input type="password" class="form-control" id="pass" path="pass" />
                    <div class="alert-danger"><strong><form:errors path="pass"/></strong></div>
                </div>
                <div class="form-group">
                    <label for="pass-rep">Powtórz Hasło</label>
                    <form:input type="password" class="form-control" id="pass-rep" path="passRep" />
                    <div class="alert-danger"><strong><form:errors path="passRep"/></strong></div>
                </div>
                <div class="submit-btn-container">
                    <button type="submit" class="btn btn-default"><strong>Zarejestruj</strong></button>
                </div>
            </form:form>
	</jsp:attribute>
</t:pagetemplate>