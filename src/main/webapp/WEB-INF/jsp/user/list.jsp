<%@ page language="java" contentType="text/html; utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head></head>
<t:pagetemplate>
	<jsp:attribute name="content">
		<h1>Użytkownicy</h1>
		<t:validatorAction />
    	<table class="table table-striped users-list">
    		<thead>
      		<tr>
        		<th>Id</th>
        		<th>Email</th>
        		<th>Login</th>
        		<th>Data utworzenia</th>
        		<th>Status</th>
        		<th>Typ Konta</th>
      		</tr>
    		</thead>
    		<tbody>
    			<c:forEach items="${users}" var="user">
    				<tr>
    					<td><c:out value="${user.id}"/></td>
    					<td><c:out value="${user.email}"/></td>
    					<td><c:out value="${user.login}"/></td>
    					<td><c:out value="${user.createddate}"/></td>
    					<td>
    						<spring:url value="/user/edit" var="userEditLink"/>
    						<form method="post" action="${userEditLink}">
    							<input type="hidden" name="userId" value="${user.id}" />
    							<select class="form-control" name="userStatus">
    								<c:forEach var="op" begin="0" end="1">
    									<c:choose>
    										<c:when test="${op==user.status}">
    											<c:set var="sel" value="selected"/>
    										</c:when>
    										<c:otherwise>
    											<c:set var="sel" value=""/>
    										</c:otherwise>
    									</c:choose>
    									<option <c:out value="${sel}"/>><c:out value="${op}"/></option>
    								</c:forEach>
    							</select>
    							<input class="btn btn-link button-save" type="submit" value="zapisz" />
    						</form>
    					</td>
    					<td>
    						<c:forEach items="${user.userUserTypes}" var="userType">
    							<c:out value="${userType.userType.type}"/>
    						</c:forEach>
    					</td>
    				</tr>
    			</c:forEach>
    		</tbody>
    	</table>
    	
	</jsp:attribute>
</t:pagetemplate>