<%@ page language="java" contentType="text/html; utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head></head>
<t:pagetemplate>
	<jsp:attribute name="content">
            <form id="login-form" name="loginForm" method="post">
                <div class="form-group">
                    <label  for="login">Login</label>
                    <input name="username" type="text" class="form-control" id="username"/>
                </div>
                <div class="form-group">
                    <label for="pass">Hasło</label>
                    <input name="password" type="password" class="form-control" id="password" />
                </div>
                
                <div class="submit-btn-container">
                    <button type="submit" class="btn btn-default"><strong>Zaloguj</strong></button>
                </div>
                <c:if test="${param.failure!=null}">
               		<label class="alert-danger"><c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" /></label>
                </c:if>
            </form>
	</jsp:attribute>
</t:pagetemplate>