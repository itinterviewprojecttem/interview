<%@ page language="java" contentType="text/html; utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
	<script src="<c:url value="/resources/js/quiz/showAnswer.js" />"></script>
</head>

<t:pagetemplate>
    <jsp:attribute name="content">
            <div class="panel-body">
                <h2><strong><c:out value="${company.name}" /></strong></h2>
                <sec:authorize access="hasRole('ROLE_USER')">
                	<a class="btn btn-primary" href="${pageContext.request.contextPath}/question/add/${company.id}">Dodaj Pytanie</a>
                </sec:authorize>
                <c:forEach var="question" items="${questions}">
                <div class="question-container">
                    <div class="question-text">
                    	<div class="answer-author">Dodano przez <span>${question.user.login}</span>, Dnia <span>${question.date}</span></div>
                    	<br />
                    	<div class="text"><p><c:out value="${question.text}"/></p></div>
                    </div>
                    <div>
                        <a id="id_link_answer_${question.id}" class="btn btn-primary" href="" 
                        	onclick="showAnswer(<c:out value="${question.id}" />);return false;">Pokaż odpowiedzi</a>
                       <a id="id_link_answer_hide_${question.id}" class="btn btn-primary" href=""
                        	onclick="hideAnswer(<c:out value="${question.id}" />);return false;" style="display:none">Ukryj Odpowiedzi</a>
                        <sec:authorize access="hasRole('ROLE_USER')">
                        	<a class="btn btn-primary" href="${pageContext.request.contextPath}/answer/add/${question.id}">Dodaj odpowiedź</a>
                        </sec:authorize>
                        <a class="btn btn-primary" href="${pageContext.request.contextPath}/question/comment/${question.id}">Komentarze</a>
                    </div>
                    <div id="id_answer_question_${question.id}" style="display: none">
                    <c:forEach var="questionAnswer" items="${question.questionAnswers}">
                    <div class="question-answer">
                    	<div class="answer-author">Dodano przez <span><!-- username --></span>, Dnia <span>${questionAnswer.answer.createddate}</span></div>
                    	<c:out value="${questionAnswer.answer.text}"/>
                        <sec:authorize access="hasRole('ROLE_USER')">
                        	<div>
                        		<spring:url value="/answer/edit" var="editUrl"/>
                        		<form method="post" action="${editUrl}">
                        			<input type="hidden" value="${questionAnswer.answer.id}" name="answerId" />
                        			<input type="hidden" value="${question.id}" name="questionId" />
                        			<input type="submit" class="btn btn-primary" value="Edytuj">
                        		</form>
                        	</div>
                       	</sec:authorize>
                    </div>
                    </c:forEach>
                    </div>
                </div>
                
                </c:forEach>
            </div>
    </jsp:attribute>
</t:pagetemplate>