<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="questionBean" scope="request" class="com.itinterview.bean.QuestionBean" />

<head>
<script src="<c:url value="/resources/js/tag/tags.js" />"></script>
</head>

<t:pagetemplate>
	<jsp:attribute name="content">
        <div class="search-title-container">
            <div id="add-company" class="search-title">Dodaj Pytanie</div>
        </div>
        <t:validatorAction />
        <form:form method="post" role="form" commandName="questionBean" action="${pageContext.request.contextPath}/question/add/${companyId}">
            <div class="form-group">
                <label for="companyDesc">Treść pytania</label>
                <form:textarea path="text" class="form-control" rows="12"></form:textarea>
                <div><label class="alert-danger"><form:errors path="text"/></label></div>
            </div>
            <div class="form-group">
                <label for="companyDesc">Treść odpowiedzi</label>
                <form:textarea path="answer" class="form-control" rows="12"></form:textarea>
                <div><label class="alert-danger"><form:errors path="answer"/></label></div>
            </div>
            <div class="submit-btn-container">
                <button type="submit" class="btn btn-default">
					<strong>Zapisz</strong>
				</button>
            </div>
    	</form:form>
	</jsp:attribute>
</t:pagetemplate>