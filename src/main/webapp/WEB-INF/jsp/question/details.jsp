<%@ page language="java" contentType="text/html; utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="commentBean" scope="request" class="com.itinterview.bean.CommentBean" />

<t:pagetemplate>
    <jsp:attribute name="content">
            <div class="panel-body">
                <div class="question-container">
                    <div class="question-text"><c:out value="${question.text}"/></div>
                    <sec:authorize access="isAuthenticated()">
                    <div>
                    	<spring:url value="/answer/add/{questionId}" var="addAnswer">
        					<spring:param name="questionId" value="${question.id}"/>
        				</spring:url>
        				<a href="${fn:escapeXml(addAnswer)}" class="btn btn-primary">Dodaj odpowiedź</a></div>
        			</sec:authorize>
                    <c:forEach items="${question.questionAnswers}" var="questionAnswers">
                    	<div class="question-answer">
                        	<c:out value="${questionAnswers.answer.text}"/>
                        	<div class="answer-author">Dodano przez <span><!-- username --></span>, Dnia <span>${questionAnswers.answer.createddate}</span></div>
                        	<sec:authorize access="isAuthenticated()">
                        		<div>
                        		<spring:url value="/answer/edit" var="editUrl"/>
                        		<form method="post" action="${editUrl}">
                        			<input type="hidden" value="${questionAnswers.answer.id}" name="answerId" />
                        			<input type="hidden" value="${question.id}" name="questionId" />
                        			<input type="submit" class="btn btn-primary" value="Edit">
                        		</form>
                        		</div>
                        	</sec:authorize>
                    	</div>
                    </c:forEach>
                </div>
                <h2>Komentarze</h2>
                <c:forEach var="comments" items="${questionComments}">
                	<div class="chat-message">
                    <div class="chat-message-info">
                        <label class="chat-message-date"><c:out value="${comments.comment.createddate}"/></label>
                        <label class="chat-message-author"><c:out value="${comments.comment.user.login}"/></label>
                    </div>
                    <div class="chat-message-text">
                        <c:out value="${comments.comment.text}"/>
                    </div>
               	 	</div>
                </c:forEach>
                <sec:authorize access="isAuthenticated()">
                <div class="chat-box">
                    <form:form method="post" role="form" commandName="commentBean" action="${pageContext.request.contextPath}/question/comment/add/${question.id}">
                        <form:textarea path="text" class="form-control" id="companyDesc" rows="5"></form:textarea>
                        <input class="btn btn-primary" type="submit" value="Post message"/>
                    </form:form>
                </div>
                </sec:authorize>
                <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
                	<div class="alert alert-info"><strong>Uwaga!</strong> Musisz być zalogowany aby dodać komentarz.</div>
                </sec:authorize>
            </div>
    </jsp:attribute>
</t:pagetemplate>