<%@ page language="java" contentType="text/html; utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="commentBean" scope="request" class="com.itinterview.bean.CommentBean" />

<t:pagetemplate>
    <jsp:attribute name="content">
                <div class="question-container">
                    <div class="question-text"><c:out value="${question.text}"/></div>
                    <c:forEach items="${question.questionAnswers}" var="questionAnswer">
                    <div class="question-answer">
                        <div><c:out value="${questionAnswer.answer.text}"/></div>
                        <div class="answer-author">Dodano przez <span><!-- username --></span>, Dnia <span>${questionAnswer.answer.createddate}</span></div>
                        <sec:authorize access="isAuthenticated()">
                        	<spring:url value="/answer/edit" var="editUrl"/>
                        		<form method="post" action="${editUrl}">
                        			<input type="hidden" value="${questionAnswer.answer.id}" name="answerId" />
                        			<input type="hidden" value="${question.id}" name="questionId" />
                        			<input type="submit" class="btn btn-primary" value="Edytuj">
                        		</form>
                        </sec:authorize>
                    </div>
                    </c:forEach>
                </div>
                <h2>Komentarze</h2>          
                <div class="chat-box">
                <c:forEach items="${comments}" var="comment">
                <div class="chat-message">
                    <div class="chat-message-info">
                        <label class="chat-message-date"><c:out value="${comment.createddate}" /></label>
                        <label class="chat-message-author"><c:out value="${comment.user.login}" /></label>
                    </div>
                    <div class="chat-message-text">
                        <c:out value="${comment.text}" />
                    </div>
                </div>
                </c:forEach>
                <sec:authorize access="isAuthenticated()">
                <div class="chat-box">
                    <form:form method="post" role="form" commandName="commentBean" action="${pageContext.request.contextPath}/question/comment/add/${question.id}">
                        <form:textarea path="text" class="form-control" id="companyDesc" rows="5"></form:textarea>
                        <input class="btn btn-primary" type="submit" value="Opublikuj komentarz"/>
                    </form:form>
                </div>
                </sec:authorize>
                <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
                	<div class="alert alert-info"><strong>Uwaga!</strong> Musisz być zalogowany aby dodać komentarz.</div>
                </sec:authorize>
				</div>
    </jsp:attribute>
</t:pagetemplate>