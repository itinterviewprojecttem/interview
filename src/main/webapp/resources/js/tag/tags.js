function addTagFromInput(){
    tag.addTag();
}

var Tag = function(){
    this.inputTagId = "companyTechs";
    this.inputDataId = "techsType";
    this.tagContainerId = "tagsContainer";
    this.tagCssClass = "tag";
    this.removeLinkLabel = " X";
    this.tagIdDelim = "id_tag_";
    this.removeLinkIdDelim= "id_remove_link_";
    this.tagValues = [];
};

Tag.prototype.create= function(){
    return new Tag();
};

Tag.prototype.addTag = function(){
    var inputTag = document.getElementById(this.inputTagId);
    var tagText= inputTag.value;
    var tagContainer = document.getElementById(this.tagContainerId);
    
    this.addTagLabel(tagText, tagContainer);
};

Tag.prototype.addTagLabel = function(tagText, tagContainer){
    if(($.inArray(tagText, this.tagValues)) > -1){
        return;
    }
    var tagLabel= document.createElement('label');
    tagLabel.innerHTML = tagText;
    tagLabel.className= this.tagCssClass;
    tagContainer.appendChild(tagLabel);
    
    var inputData = document.getElementById(this.inputDataId);
    this.tagValues[this.tagValues.length]= tagText;
    inputData.value = this.tagValues;
    
    this.addRemoveLink(tagLabel);
};

Tag.prototype.addRemoveLink= function(tagLabel){
	var tagText= tagLabel.textContent;
    var removeLink = document.createElement('a');
    removeLink.href= "";
    removeLink.innerHTML = this.removeLinkLabel;
    var thisRef= this;
    removeLink.onclick = function(){
        thisRef.removeTag(tagLabel, tagText);
        
        return false;
    };
    tagLabel.appendChild(removeLink);
};

Tag.prototype.removeTag= function(tag, tagText){
    var tagContainer = document.getElementById(this.tagContainerId);
    tagContainer.removeChild(tag);
    var indexElToRemove= this.tagValues.indexOf(tagText);
    if(indexElToRemove > -1){
        this.tagValues.splice(indexElToRemove, 1);
    }
    this.refreshInputData();
};

Tag.prototype.refreshInputData = function(){
    var inputData = document.getElementById(this.inputDataId);
    inputData.value= this.tagValues;
};

Tag.prototype.initValues= function(){
	var tagsInput= document.getElementById(this.inputDataId);
	var techs= tagsInput.value;
	if((techs==null) || (techs=="")){
		return;
	}
	
	var tagContainer = document.getElementById(this.tagContainerId);
	var techsArray= techs.split(",");
	for(i=0; i<techsArray.length; i++){
		this.addTagLabel(techsArray[i], tagContainer);
	}
}

var tag= new Tag();

$(document).ready(function(){
	tag.initValues();
});