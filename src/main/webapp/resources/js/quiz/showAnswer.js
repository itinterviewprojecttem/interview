function showAnswer(idQuestion){
	answerQuestions.showAnswer(idQuestion);
	
	return false;
}

function hideAnswer(idQuestion){
	answerQuestions.hideAnswer(idQuestion);
	
	return false;
}

var AnswerQuestion= function(){
	this.idShowAnswerLink = "id_link_answer_";
	this.idHideAnswerLink= "id_link_answer_hide_";
	this.idAnswer= "id_answer_question_";
}

AnswerQuestion.prototype.showAnswer= function(idQuestion){
	var idAnswers= this.idAnswer+idQuestion;
	var idLinkShow= this.idShowAnswerLink+idQuestion;
	var idLinkHide= this.idHideAnswerLink+idQuestion;
	
	$("#"+idAnswers).css("display", "block");
	$("#"+idLinkShow).css("display", "none");
	$("#"+idLinkHide).css("display", "");
}

AnswerQuestion.prototype.hideAnswer= function(idQuestion){
	var idAnswers= this.idAnswer+idQuestion;
	var idLinkShow= this.idShowAnswerLink+idQuestion;
	var idLinkHide= this.idHideAnswerLink+idQuestion;
	
	$("#"+idAnswers).css("display", "none");
	$("#"+idLinkShow).css("display", "");
	$("#"+idLinkHide).css("display", "none");
}

var answerQuestions= new AnswerQuestion();