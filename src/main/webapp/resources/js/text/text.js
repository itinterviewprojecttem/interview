const MAX_WIDTH= 100;
const TEXT_SUFFIX= "...";


function cut(text, elementId){
	
	var shortText= "";
	
	if(text.length < MAX_WIDTH){
		shortText= text;
	}else{
		shortText= text.substring(0, MAX_WIDTH);
	}
	var element= document.getElementById(elementId);
	var width= element.offsetWidth;
	element.innerHTML= shortText;
	console.log(width);
	console.log(shortText);
	
	return shortText;
}