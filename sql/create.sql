use itinterview;

DROP TABLE company_technolgies;
DROP TABLE technology;
DROP TABLE question_answer;
DROP TABLE answer;
DROP TABLE company_comment;
DROP TABLE question_comment;
DROP TABLE comment;
DROP TABLE question_job;
DROP TABLE job;
DROP TABLE question_company;
DROP TABLE question;
DROP TABLE company;
DROP TABLE user_user_type;
DROP TABLE user;
DROP TABLE user_type;

/***/
CREATE TABLE user_type(
	id int primary key auto_increment,
    type varchar(32) unique
);

SET SQL_SAFE_UPDATES = 0;

DELETE FROM user_type;

INSERT INTO user_type(id, type) VALUES(1, 'ROLE_USER');
INSERT INTO user_type(id, type) VALUES(2, 'ROLE_ADMIN');

/***/
/*status:
	-0 locked
    -1 unlocked*/
CREATE TABLE user(
	id int primary key auto_increment,
    email varchar(128) not null,
    login varchar(32),
    password varchar(256),
    createddate datetime,
    status int
);

CREATE TABLE user_user_type(
	id int primary key auto_increment,
    id_user int,
    id_user_type int,
    foreign key (id_user) references user(id),
    foreign key (id_user_type) references user_type(id)
);

DELETE FROM user;

INSERT INTO user(id, email, login, password, status) VALUES (1, 'kamilcio2@gmail.com', 'root', 'root', 1);
INSERT INTO user_user_type(id, id_user, id_user_type) VALUES (2, 1, 1);
INSERT INTO user_user_type(id, id_user, id_user_type) VALUES (3, 1, 2);

/***/
CREATE TABLE technology(
	id int primary key auto_increment,
    value varchar(128) unique
);

SELECT * from technology;

/***/
CREATE TABLE company(
	id int primary key auto_increment,
    name varchar(128) not null,
    website varchar(256),
    description longtext,
    id_author int,
    createddate datetime,
    foreign key(id_author) references user(id)
);

SELECT * FROM company;

/***/
CREATE TABLE company_technolgies(
	id int primary key auto_increment,
    id_company int,
    id_technology int,
    foreign key (id_company) references company(id),
    foreign key (id_technology) references technology(id)
);

SELECT * FROM company_technolgies;

/***/
CREATE TABLE job(
	id int primary key auto_increment,
    name varchar(256) unique
);

INSERT INTO job(id, name) VALUES (2, 'junior developer');
INSERT INTO job(id, name) VALUES (1, 'developer');
INSERT INTO job(id, name) VALUES (3, 'senior developer');

/***/
CREATE TABLE question(
	id int primary key auto_increment,
    text longtext not null,
    id_author int,
    createddate datetime,
    foreign key (id_author) references user(id)
);

/***/
CREATE TABLE question_company(
	id int primary key auto_increment,
    id_company int,
    id_question int,
    foreign key(id_company) references company(id),
    foreign key(id_question) references question(id)
);

/***/
CREATE TABLE question_job(
	id int primary key auto_increment,
    id_question int,
    id_job int,
    foreign key (id_question) references question(id),
    foreign key (id_job) references job(id)
);

/***/
CREATE TABLE answer(
	id int primary key auto_increment,
    text longtext,
    createddate datetime
);

/***/
CREATE TABLE question_answer(
	id int primary key auto_increment,
    id_question int,
    id_answer int,
    foreign key (id_question) references question(id),
    foreign key (id_answer) references answer(id)
);

/***/
CREATE TABLE comment(
	id int primary key auto_increment,
    text longtext not null,
    createddate datetime,
    id_author int,
    foreign key (id_author) references user(id)
);

/***/
CREATE TABLE company_comment(
	id int primary key auto_increment,
    id_company int,
    id_comment int,
    foreign key (id_company) references company(id),
    foreign key (id_comment) references comment(id)
);

SELECT question.text, comment.text FROM question, comment, question_comment
WHERE question.id= 3 AND question_comment.id_question= question.id AND question_comment.id_comment= comment.id;

/***/
CREATE TABLE question_comment(
	id int primary key auto_increment,
    id_question int,
    id_comment int,
    foreign key (id_question) references question(id),
    foreign key (id_comment) references comment(id)
);

commit;